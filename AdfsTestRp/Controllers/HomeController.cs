﻿using System;
using System.IdentityModel.Services;
using System.Threading;
using System.Web.Mvc;

namespace AdfsTestRp.Controllers
{   /// <summary>
    /// https://msdn.microsoft.com/en-us/library/hh291061(v=vs.110).aspx
    /// </summary>
    ///
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.ClaimsIdentity = Thread.CurrentPrincipal.Identity;

            return View();
        }

        public ActionResult SignOut()
        {
            var module = FederatedAuthentication.WSFederationAuthenticationModule;
            module.SignOut(false);
            var request = new SignOutRequestMessage(new Uri(module.Issuer), module.Realm);
            return new RedirectResult(request.WriteQueryString());
        }
    }
}